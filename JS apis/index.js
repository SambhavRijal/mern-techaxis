const itemContainer = document.getElementById("content");

// Ways to resolve a promise
// 1. Async await
// 2. .then

// Using async await
async function fetchAsyncData() {
  try {
    const response = await fetch("https://jsonplaceholder.typicode.com/posts");

    // response currently stores the api response, it doesnt just cotain the data
    // but also contains alot of other information. Information in internet is sent in string format
    // we need to change it back to json format (object format) that we can use for this
    // we do response.json()
    // we do await response.json() beacuse it returns a promise that resolves with the parsed JSON DATA
    // Response doesnt just contain data, it contains response header and more so parsing can take time so its treated as a, so using await means that
    // it waits for the JSON parsing to complete before proceeding with further execution

    const data = await response.json();
    console.log(data);
  } catch (error) {
    console.error("Error fetching data:", error);
  }
}

// Using .then we dont need async await because it means everything iside .then will only be
// excuted after the main action is resolved, then js goes inside .then
//  if complete .then go do the inside work
function fetchData() {
  try {
    fetch("https://jsonplaceholder.typicode.com/posts") // api for the get request
      .then((response) => response.json())
      .then((data) => showdata(data));
  } catch (error) {
    console.error("Error fetching data:", error);
  }
}

function showdata(data) {
  try {
    for (const item of data) {
      itemContainer.innerHTML +=
        "<div class='item'> <div class='itemtitle'>" +
        item.title +
        "</div> <div class='itembody'>" +
        item.body +
        "</div>  </div>";
    }
  } catch (error) {
    console.error("Error:", error);
  }
}

// fetchDataAndLog();

fetchData();
